console.log ("*** AdivinaQuienSoy ***");

let personajes = [{
    name:['mario bross','mario','fontanero','super mario'],
    foto:'mario.png',
    preguntas:['Es animal?','Es de nintendo','Tiene bigote ?','Usa Armas?','Es Viejo?'],
    respuetas:['no','si','si','no','si'],
}, {
    name:['goku','kakaroto','ozaru','son goku'],
    foto:'goku.png',
    preguntas:['Vuela?','Usa armadura ?','Tiene poderes ?','Usa Armas?','Es ser humano?'],
    respuetas:['si','no','si','no','no'],
},{
     name:['slayer','doomguy','doomslayer','marine de doom'],
    foto:'doomguy.png',
     preguntas:['Usa una sierra?','Usa armadura ?','Tiene granadas ?','Usa una espada?','Es ser humano?'],
     respuetas:['si','si','si','si','no'],
},{
    name:['canelita','canela','isabelle','shizue'],
    foto:'canelita.png',
     preguntas:['Contrata?','Trabaja ?','Tiene cascabeles ?','Usa armas?','Es un perro?'],
     respuetas:['si','si','si','no','si'],
},{
    name:['vault boy','morador','vaultboy'],
    foto:'VaultBoy.png',
    preguntas:['Promociona?','Usa cosas ?','Tiene habilidades ?','Usa Armas?','Es ser humano?'],
    respuetas:['si','no','si','si','no'],
}
];
const validarNombre = (nombre,i) => {
    let bandera = false;
    personajes[i].name.forEach(nomPer =>{
        if (nombre == nomPer) {
            bandera = true;
        }
    });
    console.log ("bandera"+ bandera)
    return bandera;
}

const btnJugar = document.getElementById("btnJugar");
const imgPersonaje = document.getElementById("imgPersonaje");
let imgResultado;
let indice;
let opacidad;
let acierto;
let incorrecto;
let puntop =0;
let punton =0;

btnJugar.addEventListener('click', () =>{

    const pregunta0 = document.getElementById("pregunta0");
    const pregunta1 = document.getElementById("pregunta1");
    const pregunta2 = document.getElementById("pregunta2");
    const pregunta3 = document.getElementById("pregunta3");
    const pregunta4 = document.getElementById("pregunta4");



    /* pregunta1 a la pregunta4 */
    indice = Math.floor(Math.random() * 5);
    imgPersonaje.src = "./assets/pic/" + personajes[indice].foto;
    opacidad = 25;
    imgPersonaje.style.filter = "blur(" + opacidad +  "px)";

    pregunta0.value = personajes[indice].preguntas[0];
    pregunta1.value = personajes[indice].preguntas[1];
    pregunta2.value = personajes[indice].preguntas[2];
    pregunta3.value = personajes[indice].preguntas[3];
    pregunta4.value = personajes[indice].preguntas[4];
     /* cargar el resto de pregutas */ 
     rta0.disabled = false;
     rta1.disabled = false;
     rta2.disabled = false;
     rta3.disabled = false;
     rta4.disabled = false;
     document.getElementById("icoRta0").src = "";
     document.getElementById("icoRta1").src = "";
     document.getElementById("icoRta2").src = "";
     document.getElementById("icoRta3").src = "";
     document.getElementById("icoRta4").src = "";
     rta0.selectedIndex = 0;
     rta1.selectedIndex = 0;
     rta2.selectedIndex = 0;
     rta3.selectedIndex = 0;
     rta4.selectedIndex = 0;
     acierto=0;
})


// progar las respuestas recividas en el select

const rta0 = document.getElementById("rta0");

rta0.addEventListener('change',()=> {

        if(rta0.value == personajes[indice].respuetas[0]) {
            acierto=acierto+1;
            puntop= puntop+10;
            document.getElementById('acierto').innerHTML = acierto;
            document.getElementById('punto').innerHTML = puntop;
            opacidad = opacidad - 5;
            imgPersonaje.style.filter = "blur(" + opacidad +  "px)";
            document.getElementById("icoRta0").src = "./assets/pic/si.png";

        } else {
            incorrecto=incorrecto-1;
            punton= punton-10;
            document.getElementById('incor').innerHTML = incorrecto;
            document.getElementById('puntomal').innerHTML = punton;
            document.getElementById("icoRta0").src = "./assets/pic/no.png";
        }

        rta0.disabled = true;
})

const rta1 = document.getElementById("rta1");

rta1.addEventListener('change',()=> {

        if(rta1.value == personajes[indice].respuetas[1]) {
            acierto=acierto+1;
            puntop= puntop+10;
            document.getElementById('acierto').innerHTML = acierto;
            document.getElementById('punto').innerHTML = puntop;
            opacidad = opacidad - 5;
            imgPersonaje.style.filter = "blur(" + opacidad +  "px)";
            document.getElementById("icoRta1").src = "./assets/pic/si.png";

        } else {
            incorrecto=incorrecto-1;
            punton= punton-10;
            document.getElementById("icoRta1").src = "./assets/pic/no.png";
        }

        rta1.disabled = true;
})


const rta2 = document.getElementById("rta2");

rta2.addEventListener('change',()=> {

        if(rta2.value == personajes[indice].respuetas[2]) {
            acierto=acierto+1;
            puntop= puntop+10;
            document.getElementById('acierto').innerHTML = acierto;
            document.getElementById('punto').innerHTML = puntop;
            opacidad = opacidad - 5;
            imgPersonaje.style.filter = "blur(" + opacidad +  "px)";
            document.getElementById("icoRta2").src = "./assets/pic/si.png";

        } else {
            incorrecto=incorrecto-1;
            punton= punton-10;
            document.getElementById("icoRta2").src = "./assets/pic/no.png";
        }

        rta2.disabled = true;
})

const rta3 = document.getElementById("rta3");

rta3.addEventListener('change',()=> {

        if(rta3.value == personajes[indice].respuetas[3]) {
            acierto=acierto+1;
            puntop= puntop+10;
            document.getElementById('acierto').innerHTML = acierto;
            document.getElementById('punto').innerHTML = puntop;
            opacidad = opacidad - 5;
            imgPersonaje.style.filter = "blur(" + opacidad +  "px)";
            document.getElementById("icoRta3").src = "./assets/pic/si.png";

        } else {
            incorrecto=incorrecto-1;
            puntno= punton-10;
            document.getElementById("icoRta3").src = "./assets/pic/no.png";
        }

        rta3.disabled = true;
})

const rta4 = document.getElementById("rta4");

rta4.addEventListener('change',()=> {

        if(rta4.value == personajes[indice].respuetas[4]) {
            acierto=acierto+1;
            puntop= puntop+10;
            document.getElementById('acierto').innerHTML = acierto;
            document.getElementById('punto').innerHTML = puntop;
            opacidad = opacidad - 5;
            imgPersonaje.style.filter = "blur(" + opacidad +  "px)";
            document.getElementById("icoRta4").src = "./assets/pic/si.png";

        } else {
            incorrecto=incorrecto-1;
            punton= punton-10;
            document.getElementById("icoRta4").src = "./assets/pic/no.png";
        }

        rta4.disabled = true;
})



const btnRespuesta = document.getElementById("btnRespuesta");

btnRespuesta.addEventListener('click' , () => {

    const RespuestaGeneral = document.getElementById("RespuestaGeneral").value
    nombreIn = RespuestaGeneral.toLowerCase()
    console.log ("RespuestaGeneral"+ nombreIn)

    console.log ("Respuesta General " + nombreIn)
    if (validarNombre(nombreIn,indice)) {
        console.log (" GANASTE ");
        imgResultado="./assets/pic/win.png";
        alert(" GANASTE ");

    } else {
       
         console.log (" GAME OVER ");
         imgResultado="./assets/pic/perdio.png";
         alert(" PERDIO ");

    }
 
})